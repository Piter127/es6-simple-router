const webpack 		= require('webpack');
const path 			= require('path');

module.exports = {
	entry: [
		'./src/app.js'
	],
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'build')
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				use: [{
                    loader: 'babel-loader'
                }]

			}
		]
	},
	stats: {
        colors: true
    },
    devtool: 'source-map',
}