/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/app.js":
/*!********************!*\
  !*** ./src/app.js ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _controller = __webpack_require__(/*! ./controller */ "./src/controller.js");

var _controller2 = _interopRequireDefault(_controller);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var App = function App() {
	_classCallCheck(this, App);

	this.controller = new _controller2.default();
};

var app = new App();

/***/ }),

/***/ "./src/basket.js":
/*!***********************!*\
  !*** ./src/basket.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Basket = function Basket() {
	var _this = this;

	_classCallCheck(this, Basket);

	this.showCartAction = function () {
		_this.wrapper.innerHTML = 'index';
	};

	this.showCartListAction = function () {
		_this.wrapper.innerHTML = 'list';
	};

	this.wrapper = document.getElementById('app');
};

exports.default = Basket;

/***/ }),

/***/ "./src/controller.js":
/*!***************************!*\
  !*** ./src/controller.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _router = __webpack_require__(/*! ./router */ "./src/router.js");

var _router2 = _interopRequireDefault(_router);

var _basket = __webpack_require__(/*! ./basket */ "./src/basket.js");

var _basket2 = _interopRequireDefault(_basket);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Controller = function () {
	function Controller() {
		_classCallCheck(this, Controller);

		this.router = new _router2.default();
		this.basket = new _basket2.default();

		this.addRoutes();
		this.initRouter();
	}

	_createClass(Controller, [{
		key: 'addRoutes',
		value: function addRoutes() {
			this.router.addRoute({
				pathname: '/basket',
				handler: this.basket.showCartAction
			});

			this.router.addRoute({
				pathname: '/basket/list',
				handler: this.basket.showCartListAction
			});
		}
	}, {
		key: 'initRouter',
		value: function initRouter() {
			this.router.run();
		}
	}]);

	return Controller;
}();

exports.default = Controller;

/***/ }),

/***/ "./src/router.js":
/*!***********************!*\
  !*** ./src/router.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Router = function () {
	function Router() {
		_classCallCheck(this, Router);

		this.routes = [];
		this._activeRoute = null;
	}

	_createClass(Router, [{
		key: 'getRoutes',
		value: function getRoutes() {
			return this.routes;
		}

		/**
  	{
  		pathname: string,
  		handler: function
  	}
  	 */

	}, {
		key: 'addRoute',
		value: function addRoute(route) {
			if (!this.routeIsUnique(route)) {
				throw Error('Route ' + route.pathname + ' is not unique!');
			}

			this.routes.push(route);
		}
	}, {
		key: 'routeIsUnique',
		value: function routeIsUnique(newRoute) {
			var routes = this.getRoutes();

			return !routes.find(function (route) {
				return route.pathname === newRoute.pathname;
			});
		}

		/**
   * Remove first and last slashes from pathnames
   */

	}, {
		key: 'routeIsMatched',
		value: function routeIsMatched(currentPath, route) {
			currentPath = currentPath.replace(/^\/|\/$/g, '');
			var routePath = route.pathname.replace(/^\/|\/$/g, '');

			return currentPath === routePath;
		}
	}, {
		key: 'getCurrentPathname',
		value: function getCurrentPathname() {
			return window.location.pathname;
		}

		/**
   * @todo render error page if route not exist
   */

	}, {
		key: 'run',
		value: function run() {
			var _this = this;

			var routes = this.getRoutes();
			var currentPath = this.getCurrentPathname();

			var matchedRoute = routes.find(function (route) {
				return _this.routeIsMatched(currentPath, route);
			});

			if (matchedRoute) {
				this.activeRoute = matchedRoute;
				return matchedRoute.handler();
			}

			/**
    * Render error  page
    */
			this.activeRoute = null;
			throw Error('Route does not exist');
		}
	}, {
		key: 'activeRoute',
		get: function get() {
			return this._activeRoute;
		},
		set: function set(route) {
			this._activeRoute = route;
		}
	}]);

	return Router;
}();

exports.default = Router;

/***/ }),

/***/ 0:
/*!**************************!*\
  !*** multi ./src/app.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./src/app.js */"./src/app.js");


/***/ })

/******/ });
//# sourceMappingURL=main.js.map