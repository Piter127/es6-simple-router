export default class Router {
	
	constructor() {
		this.routes = [];
		this._activeRoute = null;
	}

	getRoutes() {
		return this.routes;
	}

	/**
		{
			pathname: string,
			handler: function
		}
 	 */
	addRoute(route) {
		if(!this.routeIsUnique(route)) {
			throw Error(`Route ${route.pathname} is not unique!`);
		}

		this.routes.push(route);
	}

	get activeRoute() {
		return this._activeRoute;
	}

	set activeRoute(route) {
		this._activeRoute = route;
	}

	routeIsUnique(newRoute) {
		const routes = this.getRoutes();

		return !routes.find(route => route.pathname === newRoute.pathname);
	}

	/**
	 * Remove first and last slashes from pathnames
	 */
	routeIsMatched(currentPath, route) {
		currentPath = currentPath.replace(/^\/|\/$/g, '');
		const routePath = route.pathname.replace(/^\/|\/$/g, '');

		return currentPath === routePath;

	}

	getCurrentPathname() {
		return window.location.pathname;
	}

	/**
	 * @todo render error page if route not exist
	 */
	run() {
		const routes = this.getRoutes();
		const currentPath = this.getCurrentPathname();

		const matchedRoute = routes.find(route => this.routeIsMatched(currentPath, route));

		if(matchedRoute) {
			this.activeRoute = matchedRoute;
			return matchedRoute.handler();
		}

		/**
		 * Render error  page
		 */
		this.activeRoute = null;
		throw Error('Route does not exist');
	}

}