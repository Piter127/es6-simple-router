import Router from './router';
import Basket from './basket';

export default class Controller {
	constructor() {
		this.router = new Router();
		this.basket = new Basket();

		this.addRoutes();
		this.initRouter();
	}

	addRoutes() {
		this.router.addRoute({
			pathname: '/basket',
			handler: this.basket.showCartAction
		});

		this.router.addRoute({
			pathname: '/basket/list',
			handler: this.basket.showCartListAction
		});
	}

	initRouter() {
		this.router.run();
	}
}