export default class Basket {
	constructor() {
		this.wrapper = document.getElementById('app');
	}

	showCartAction = () => {
		this.wrapper.innerHTML = 'index';
	}

	showCartListAction = () => {
		this.wrapper.innerHTML = 'list';
	}
}